# tool_kicad2components.py (CSV file converter for TronStol A1)
# Copyright: 2024 Bradan Lane STUDIO
# Open Source License: MIT

""" --------------------------------------------------------------------------

kicad2components.numbers for TronStol A1 (aka Neoden S1)

This tool will generate a spreadsheet for all of the components on a PCB.
It will include the number of each component and the nozzle that is required.

For components already installed on a machine, it will set defaults for
the machine and feeder position.

The administrator will update this file as they install feeders onto one or
more pick-and-place machines.

The resulting file will be used to generate the machine job file(s).

The tool will also verify if the component is in inventory and generate
a list of components that need to be ordered and/or installed onto feeders.


The updated map file will be used by kicad2job to update the components
spreadsheet. The map file will also be used to generate the machine job file(s).

inputs: a kicad position file (CSV)
outputs: a spreadsheet for machine setup

-------------------------------------------------------------------------- """

DEFAULT_OUTPUT_FILE = 'jobfile'

import os

import argparse
import json
import xml.etree.ElementTree as ET
import io

import tool_sheet_functions as tsf
import tool_fiducial_image as tfi


# ---------------------------------------------------------------------------
# ---------------------------------------------------------------------------

def adjust_for_rotation(args, x, y, simple=False):
    # simple rotation for board stuff
    if simple:
        if args.do_rotation:
            return y, x
        return x, y
    
    if args.do_rotation:
        px, py = y, args.pcb_width - x
        if (py < 0):
            print(f"Error: using width = {args.pcb_width:4.2f} resulted in negative position at ({px:4.2f}, {py:4.2f}) ...")
        px += args.panel_offset_x
        py += args.panel_offset_y
    else:
        px = x + args.panel_offset_x
        py = y + args.panel_offset_y
    return px, py

def make_int(value):
    try:
        return int(value)
    except:
        return None
# ---------------------------------------------------------------------------

def add_parameter(parent, name, value, default=None):
    if value == None:
        if default == None:
            value = 'default'
        else:
            value = default
    if isinstance(value, float):
        value = f"{value:4.2f}"
    elif isinstance(value, int):
        value = str(value)
    elif isinstance(value, bool):
        value = str(value).lower()
        
    param = ET.SubElement(parent, name)
    param.text = value

def add_footprint(db, xml, item):
    child = ET.Element('footprint')
    add_parameter(child, 'name', item[0])
    add_parameter(child, 'length', f"{item[1]:4.2f}")
    add_parameter(child, 'width', f"{item[2]:4.2f}")
    add_parameter(child, 'thickness', f"{item[3]:4.2f}")
    xml.append(child)

def add_stack(db, xml, item):
    child = ET.Element('stack')
    child.set('id', str(item[1]))
    add_parameter(child, 'description', item[2], '...')
    add_parameter(child, 'footprint_name', item[3], 'null')
    add_parameter(child, 'match_rate', make_int(item[4]))
    add_parameter(child, 'feeding_rate', make_int(item[5]))
    offset = '0/0' if (item[6] == None or item[7] == None) else f"{item[6]:4.2f}/{item[7]:4.2f}"
    add_parameter(child, 'pick_offset', offset)
    add_parameter(child, 'pick_height', item[8])
    add_parameter(child, 'pick_delay', make_int(item[9]))
    add_parameter(child, 'place_height', item[10])
    add_parameter(child, 'place_delay', make_int(item[11]))
    add_parameter(child, 'safe_height', item[12])
    add_parameter(child, 'alignment_method', item[13])
    add_parameter(child, 'laser_range', item[14])
    add_parameter(child, 'laser_height', item[15])
    add_parameter(child, 'option', item[16])
    add_parameter(child, 'skip', item[17], 'false')
    add_parameter(child, 'pick_retry', make_int(item[18]))
    rotation = 0 if (() or (item[1] == 'default') or (item[1] < 30) or (item[1] > 58)) else 180
    add_parameter(child, 'initial_rotation', rotation, 0)
    # change nozzle availability based on feeder position (aka 'id'): LR order is 4-3-2-1
    if isinstance(item[1], str):
        nozzle_range = '1/2/3/4/'
    else:
        pos = int(item[1])
        if   pos in [1,2,57,58]:        nozzle_range = '/4/'
        elif pos in [28,29,30,31]:      nozzle_range = '/1/'
        elif pos in [3,4,55,56]:        nozzle_range = '/3/4/'
        elif pos in [26,27,32,33]:      nozzle_range = '/1/2/'
        elif pos in [5,6,53,54]:        nozzle_range = '/2/3/4/'
        elif pos in [24,25,34,35]:      nozzle_range = '/1/2/3/'
        else:                           nozzle_range = '/1/2/3/4/'
    add_parameter(child, 'nozzle_range', nozzle_range, '/1/2/3/4/')
    xml.append(child)

def add_special_stack(db, xml, item):
    #print(item)
    child = ET.Element('special_stack')
    child.set('id', str(item[1]))
    add_parameter(child, 'description', item[2], '...')
    add_parameter(child, 'footprint_name', item[3], 'null')
    add_parameter(child, 'match_rate', item[4])
    offset = '0/0' if (item[5] == None or item[6] == None) else f"{item[5]:4.2f}/{item[6]:4.2f}"
    add_parameter(child, 'pick_offset', offset)
    add_parameter(child, 'pick_height', item[7])
    add_parameter(child, 'pick_delay', item[8])
    add_parameter(child, 'place_height', item[9])
    add_parameter(child, 'place_delay', item[10])
    add_parameter(child, 'safe_height', item[11])
    add_parameter(child, 'alignment_method', item[12])
    add_parameter(child, 'laser_range', item[13])
    add_parameter(child, 'laser_height', item[14])
    add_parameter(child, 'option', item[15])
    add_parameter(child, 'skip', item[16], 'false')
    add_parameter(child, 'pick_retry', item[17])
    offset = '0/0' if (item[18] == None or item[19] == None) else f"{item[18]:4.2f}/{item[19]:4.2f}"
    add_parameter(child, 'matrix_offset', offset)
    offset = '0/0' if (item[20] == None or item[21] == None) else f"{make_int(item[20]):d}/{make_int(item[21]):d}"
    add_parameter(child, 'matrix_quantity', offset)
    add_parameter(child, 'next_position', make_int(item[22]), 1)
    # change nozzle availability based on feeder position (aka 'id'): LR order is 4-3-2-1
    pos = int(item[1])
    if   pos in range(101, 110):    nozzle_range = '/4/'
    elif pos in range(110, 120):    nozzle_range = '/3/4/'
    elif pos in range(120, 140):    nozzle_range = '/2/3/4/'
    elif pos in range(160, 180):    nozzle_range = '/1/2/3/'
    elif pos in range(180, 190):    nozzle_range = '/1/2/'
    elif pos in range(190, 199):    nozzle_range = '/1/'
    else:                           nozzle_range = '/1/2/3/4/'
    # nozzle_range = '/1/2/3/4/' # for now, we will just enable all nozzles
    add_parameter(child, 'nozzle_range', nozzle_range, '/1/2/3/4/')
    xml.append(child)
    
# ---------------------------------------------------------------------------

def add_pcb(args, xml):
    bw, bh = adjust_for_rotation(args, args.board_width, args.board_height, simple=True)
    pw, ph = adjust_for_rotation(args, args.pcb_width, args.pcb_height, simple=True)
    
    child = ET.Element('pcb')
    add_parameter(child, 'length', bh)
    add_parameter(child, 'width', bw)
    add_parameter(child, 'color', args.color)
    add_parameter(child, 'feeding_position', '100/12')
    add_parameter(child, 'panel_size', f"{ph:5.2f}/{pw:5.2f}")
    if args.format == 'panel':
        add_parameter(child, 'fiducial_option', 'global')
    else:
        add_parameter(child, 'fiducial_option', 'local')
    xml.append(child)

def add_panel(args, xml, x, y):
    #adjust for rotation is handled before this call
    
    child = ET.Element('panel')
    add_parameter(child, 'origin', f"{x:4.2f}/{y:4.2f}")
    add_parameter(child, 'rotation', 0)
    add_parameter(child, 'offset', '0/0')
    add_parameter(child, 'skip', 'false')
    xml.append(child)


def add_fiducial(db, args, pnp, xml, x, y, name=None):
    x, y = adjust_for_rotation(args, x, y)
    
    fid = ET.Element('fiducial')
    add_parameter(fid, 'coordinate', f"{x:4.2f}/{y:4.2f}")
    
    found = False
    skip = False
    if db.defined_fiducials is not None:
        for fiducial in db.defined_fiducials:
            #print(f"checking fiducial {fiducial['name']} on {fiducial['pnp']} with {name} on {pnp}")
            if fiducial['name'] == name and fiducial['pnp'] == pnp:
                print(f'Using defined fiducial {name} on pnp{pnp} at {x:4.2f}/{y:4.2f}')
                add_parameter(fid, 'image', fiducial['image'])
                found = True
                break
        if not found:
            # if we have defined fiducials, then we will skip any that will be populated with generic image data
            skip = True
    if not found:
        #print(f'using generic fiducial {name} at {x:4.2f}/{y:4.2f}')
        if skip:
            print(f'Skipping      fiducial {name} on pnp{pnp} at {x:4.2f}/{y:4.2f}')
        else:            
            print(f'Using generic fiducial {name} on pnp{pnp} at {x:4.2f}/{y:4.2f}')
        add_parameter(fid, 'skip', ('true' if skip else 'false'))
        add_parameter(fid, 'image', tfi.fiducial_image)
    xml.append(fid)

def add_component(args, db, xml, component, pnp):
    # pnp, feeder_position, reference, value, nozzle, coord_x, coord_y, rotation, str(skip)
    if component[0] != pnp:
        return

    x, y = adjust_for_rotation(args, component[5], component[6])

    #add optional custom rotation from db.inventory    
    stack = tsf.find_stack_by_name(db, pnp, component[3])

    rotation = component[7] + (90.0 if args.do_rotation else 0)

    # stacks 30..58 are on the back of the machine and thus rotated 180 degrees
    if component[3] > 29 and component[3] < 59:
        rotation += 180.0

    # stack vs special stack data layout
    if component[3] < 100:
        custom_rotation = stack[19]
    else:
        custom_rotation = stack[23]

    if custom_rotation is not None:
        #print(f'custom rotation of {custom_rotation:4.2f}° for {component[1]:4s} using stack position {component[3]:03d}')
        #print(stack)
        rotation += custom_rotation

    while rotation > 180.0:
        rotation -= 360

    comp = ET.Element('comp')
    add_parameter(comp, 'designator', component[1])
    add_parameter(comp, 'stack_id', component[3])
    add_parameter(comp, 'nozzle', component[4])
    add_parameter(comp, 'rotation', f'{rotation:4.2f}')
    add_parameter(comp, 'coordinate', f'{x:4.2f}/{y:4.2f}')
    add_parameter(comp, 'skip', 'false')
    xml.append(comp)
 
# ---------------------------------------------------------------------------
 
def format_xml(element, level=0):
    # pretty print XML
    i = "\n" + level*"\t"
    if len(element):
        if not element.text:
            element.text = i + "\t"
        if not element.tail or not element.tail.strip():
            element.tail = i
        for element in element:
            format_xml(element, level+1)
        if not element.tail or not element.tail.strip():
            element.tail = i
    else:
        if level and (not element.tail or not element.tail.strip()):
            element.tail = i

# ---------------------------------------------------------------------------

def find_available_nozzle(values, availables, size_needed, positions):
    # need to filter the nozzle_value list by the nozzle_avail list and the nozzle_positions list
    values_copy = [ value if avail and (i + 1) in positions else 0
                    for i, (value, avail) in enumerate(zip(values, availables))
                  ]
    #print("searching for nozzle: ", size_needed, values_copy, availables)
    for i, size in enumerate(values_copy):
        if size == size_needed and availables[i] == 1:
            #print("found: available nozzle in position ", i)
            return i
    return -1

# ---------------------------------------------------------------------------

def write_output(db, args, pnp):
    # TODO we will need to add number of machines to use to the JSON and then do the following for each
    # create the output xml base content
    xml = ET.Element('smart_work')

    add_pcb(args, xml)

    orig_x, orig_y = adjust_for_rotation(args, args.panel_origin_x, args.panel_origin_y, simple=True)
    pcb_w, pcb_h = adjust_for_rotation(args, args.pcb_width, args.pcb_height, simple=True)
    spacing_x, spacing_y = adjust_for_rotation(args, args.panel_spacing_x, args.panel_spacing_y, simple=True)
    
    pos_y = orig_y
    # output panels to XML
    for rows in range(args.panel_y):
        pos_x = orig_x
        for cols in range(args.panel_x):
            add_panel(args, xml, pos_x, pos_y)
            pos_x += pcb_w + spacing_x
        pos_y += pcb_h + spacing_y
    
    # add the footprints (the first should be the null footprint): name, length, width, thickness
    child = ET.Element('footprint')
    for footprint in db.footprints:
        add_footprint(db, xml, footprint)

    # add the default stack first
    item = tsf.find_stack_by_name(db, pnp, 'default')
    if item == None:
        print('Error: default stack not found')
        return
    add_stack(db, xml, item)    # add the 'default'
    
    # add stacks 1..58 (adding placeholders for empty slots)
    for i in range(58):
        item = tsf.find_stack_by_name(db, pnp, i+1)
        if item is None or str(item[1]).startswith('*'):
            #print(f'using default stack for {i+1}')
            item = [0, i+1] + [None] * 20
        add_stack(db, xml, item)
    
    # add the special_stacks 101..199 (adding placeholders for empty slots)
    for i in range(99):
        item = tsf.find_stack_by_name(db, pnp, i+101)
        if item is None or str(item[1]).startswith('*'):
            #print(f'using default stack for {i+101}')
            item = [0, i+101] + [None] * 24
        add_special_stack(db, xml, item)
    
    # add the fiducials and include the fiducial_image
    for fiducial in db.fiducials:
        add_fiducial(db, args, pnp, xml, fiducial[0], fiducial[1], fiducial[2])

    # -----------------------------------------------------------------------
    # now that all of the static content has been written, we will add the components
    # -----------------------------------------------------------------------
    
    '''
    TBD - sorting is different for M2 when doing 'panel at a time'
        we need to sort the components by stack number for M1 since all nozzles are teh same
        but M2 has all different nozzles we we want to sort by stack and component to interweave the nozzle usage
    '''
    
    db.components.sort(key=lambda x: (x[0], x[3]))  # sort components by pnp and stack number
    db.nozzles.sort(key=lambda x: (x[0], x[1]))     # sort nozzles by pnp and stack number
    
    # assign nozzles to the components
    #print("nozzles installed:", db.nozzles_installed)
    nozzle_avails = [1,1,1,1]    # 1, 2, 3, 4 (even thought the head is 4, 3 ,2, 1)
    nozzle_values = ['','','','']
    for nozzle in db.nozzles_installed:
        if nozzle[0] == pnp:
            nozzle_values[int(nozzle[1])-1] = int(nozzle[2])
    
    if len(db.components) != len(db.nozzles):
        print('Error: number of components and nozzle mappings do not match')
        return

    #print()
    #print(db.components[0])
    #print(nozzle_value)
    #print(db.nozzles)
    
    '''
    optimizing nozzles is very complicated and ideally would look something like the following:
    loop through components
        the last element of a component is its output order
        if the output order is 0 it means it has not been assigned a nozzle
        get a list of acceptable nozzle positions
        loop through the nozzles and assign the first one that is available
        if no nozzles are available then loop forward through components
        and assign the first one that is not assigned and has an available nozzle
        if none are found, then reset available and use the first available nozzle

    but that turned into a nightmare so we are going to do something much simpler
        assign the next acceptable nozzle and if there are none, the reset and continue
    '''

    for i, component in enumerate(db.components):
        if component[0] != pnp:
            continue

        # get acceptable nozzle positions
        size_needed = db.nozzles[i][2]
        nozzle_positions = db.nozzles[i][3]
        
        #print(F"Processing   {i:02d}: PNP{component[0]:d} Ref: {component[1]:4s} Part: {component[2]:12s} :: {size_needed} {nozzle_positions} {nozzle_avails}")

        position = find_available_nozzle(nozzle_values, nozzle_avails, size_needed, nozzle_positions)
        if position >= 0:
            #print("found", i, "in available nozzles:", size_needed, nozzle_values, position+1)
            component[4] = position+1
            nozzle_avails[position] = 0
        else:
            # reset and use first available
            nozzle_avails = [1,1,1,1]
            #print(F"ReProcessing {i:02d}: PNP{component[0]:d} Ref: {component[1]:4s} Part: {component[2]:12s} :: {size_needed} {nozzle_positions} {nozzle_avails}")
            position = find_available_nozzle(nozzle_values, nozzle_avails, size_needed, nozzle_positions)
            if position >= 0:
                #print("found", i, "in reset nozzles:", size_needed, nozzle_values, position+1)
                component[4] = position+1
                nozzle_avails[position] = 0
        if position < 0:
            print(F"Warning: no nozzle available. Skipping component: PNP{component[0]:d} Ref: {component[1]:4s} Part: {component[2]:12s}")
            # rather than return, we will skip this component and continue
        else:
            add_component(args, db, xml, component, pnp)
        #print()
 
 
    # -----------------------------------------------------------------------
    # write the components to the output file
    # -----------------------------------------------------------------------

    format_xml(xml)

    pretty_xml = ET.tostring(xml, encoding='utf-8').decode('utf-8')
    
    # output to the same folder as the input file
    outfilefull = os.path.join(db.projectpath, args.outfile) + '_m' + str(pnp)
    with open(outfilefull+'.xml', "w") as f:
        f.write(pretty_xml)


def parse_args_simple():
    cmdline_args = argparse.ArgumentParser()
    cmdline_args.add_argument("infile", help="name of KiCAD CSV position file")
    cmdline_args.add_argument("outfile", nargs='?', help="(optional) base name of spreadsheet file without extension", default=DEFAULT_OUTPUT_FILE)

    args = cmdline_args.parse_args()
    return args

def parse_args():
    cmdline_args = argparse.ArgumentParser()
    cmdline_args.add_argument("infile", help="name of KiCAD CSV position file")
    cmdline_args.add_argument("outfile", nargs='?', help="(optional) base name of spreadsheet file without extension", default=DEFAULT_OUTPUT_FILE)
    cmdline_args.add_argument("--config", dest="config", nargs=1, help="name of JSON configuration file with optional folder path", required=True)

    args = cmdline_args.parse_args()
    args.config = args.config[0] if isinstance(args.config, list) else args.config

    try:
        with open(args.config) as json_file:
            config_data = json.load(json_file)
            #print(config_data)

            '''
                The 'width' of a PCB is teh space between the conveyor rails.
                The 'height' of a PCB is the distance along the conveyor.
                If a PCB is tall, then it must be rotated.
                If a PCB has a camera band along the top, then it must be rotated.
            '''
            args.do_rotation = config_data.get('rotate', False)
            args.pnps = config_data.get('machines', 1)
            args.format = config_data.get('format', 'board')	# board at a time or whole panel at once

            # look for board object and parse its children
            pair = config_data.get('board', None)
            if pair is None:
                print("Missing board declaration")
                sys.exit()
            args.board_height = pair.get('height', 0)
            args.board_width = pair.get('width', 0)
            args.color = pair.get('color', 'black')
            if (not args.board_width) or (not args.board_height):
                print("Missing board dimensions (%.3f/%.3f)" % (args.board_width,args.board_height))
                sys.exit()

            # look for offset object and parse its children
            pair = config_data.get('offset', None)
            if pair is None:
                print("Missing pcb offset")
                args.offset_x = 0.0
                args.offset_y = 0.0
            else:
                args.offset_x = pair.get('x', 0.0)
                args.offset_y = pair.get('y', 0.0)


            # look for panel object and parse its children ... and children of children
            panel = config_data.get('panel', None)
            if panel is None:
                print("Warning: Missing panel declaration")
                args.has_panel = False
                args.panel_x = 1
                args.panel_y = 1
                args.pcb_height = args.board_height
                args.pcb_width = args.board_width
                args.panel_spacing_x  = 0.0
                args.panel_spacing_y  = 0.0
                args.panel_origin_x = 0.0
                args.panel_origin_y = 0.0
            else:
                args.has_panel = True
                args.panel_x = panel.get('cols', 1)
                args.panel_y = panel.get('rows', 1)

                # look for pcb object and parse its children
                pair = panel.get('pcb', None)
                if pair is None:
                    print("Missing panel pcb declaration")
                    sys.exit()
                args.pcb_height = pair.get('height', 0)
                args.pcb_width = pair.get('width', 0)
                if (not args.pcb_width) or (not args.pcb_height):
                    print("Missing panel pcb dimensions (%.3f/%.3f)" % (args.pcb_width,args.pcb_height))
                    sys.exit()

                # look for spacing between panels
                pair = panel.get('spacing', None)
                if pair is None:
                    print("Missing panel spacing")
                    args.panel_spacing_x = 0.0
                    args.panel_spacing_y = 0.0
                elif isinstance(pair, (int, float)):
                    args.panel_spacing_x = pair
                    args.panel_spacing_y = pair
                else:
                    args.panel_spacing_x = pair.get('x', 0.0)
                    args.panel_spacing_y = pair.get('y', 0.0)

                # look for origin of the first panel relative to the board (edge rails and any spacing to first PCB)
                pair = panel.get('origin', None)
                if pair is None:
                    print("Missing panel origin")
                    args.panel_origin_x = 0.0
                    args.panel_origin_y = 0.0
                else:
                    args.panel_origin_x = pair.get('x', 0.0)
                    args.panel_origin_y = pair.get('y', 0.0)

                # look for offset correction for positions on the panel
                pair = panel.get('offset', None)
                if pair is None:
                    print("Missing panel offset")
                    args.panel_offset_x = 0.0
                    args.panel_offset_y = 0.0
                else:
                    args.panel_offset_x = pair.get('x', 0.0)
                    args.panel_offset_y = pair.get('y', 0.0)
        
    except IOError:
        print("Error: opening configuration file '%s'" % (args.config))
        sys.exit()

    #print (args)
    return args


def main():
    args = parse_args()
    
    # Read in existing components data from the spreadsheets
    db = tsf.load_components()
    tsf.load_predefined_fiducials(db, "fiducials.json") # adds to db

    tsf.load_and_process_csv(db, args, deduplicate=False) # adds to db

    outfilefull = os.path.join(db.projectpath, 'mapper')
    #outfilefull = os.path.join(os.getcwd(), 'mapper') 
    table = tsf.load_sheet(outfilefull, 'nozzles') # def load_sheet(filename, sheet_name)
    db.nozzles_installed = tsf.get_table_data(table, skip_astrisk=False)

    for pnp in range(args.pnps):
        write_output(db, args, pnp+1)
    print()

# start execution here
main()
