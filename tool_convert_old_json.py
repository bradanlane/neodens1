#define toggle between output of Microsoft Excel ".xlsx" file or Apple Numbers ".numbers" file
OUTPUT_FORMAT = 'numbers'  # 'xlsx' or 'numbers'

import xml.etree.ElementTree as ET
import itertools


# ---------------------------------------------------------------------------
if OUTPUT_FORMAT == 'numbers':
    import numbers_parser as np
else:
    from openpyxl import wb
# ---------------------------------------------------------------------------

first_sheet = True   # global to know if there is a default sheet to write to
def create_output():
    if OUTPUT_FORMAT == 'numbers':
        return np.Document(num_header_cols=0, num_rows=1, num_cols=1)
    else:
        return wb()

def create_sheet(doc, sheet_name, title="Table 1"):
    global first_sheet
    
    if OUTPUT_FORMAT == 'numbers':
        # numbers has a workbook with 1 or more sheets which may have 1 or more tables
        if first_sheet:
            first_sheet = False
            sheet = doc.sheets[0]
            sheet.name = sheet_name
            table = sheet.tables[0]
            table.name = title
        else:
            doc.add_sheet(sheet_name, title, num_rows=1, num_cols=1)
            sheet = doc.sheets[sheet_name]
            table = sheet.tables[0]
            table.num_header_cols = 0
        return table
    else:
        # excel has a workbook with 1 or more sheets with no tables
        sheet = doc.active
        sheet.title = sheet_name
        return sheet
    
def write_cell(table, row, col, value, is_convertible = False):
    if isinstance(value, str):
        v = value.replace('"', '')
        if is_convertible:
            try:
                v = int(v)
            except:
                try:
                    v = float(v)
                except:
                    v = value.replace('"', '')
                    pass
    else:
        v = value
    if OUTPUT_FORMAT == 'numbers':
        table.write(row, col, v)
    else:
        table.cell(row=row+1, column=col+1).value = v

def save_output(doc, filename):
    if OUTPUT_FORMAT == 'numbers':
        doc.save(filename+'.numbers')
    else:
        doc.save(filename+'.xlsx')

# ---------------------------------------------------------------------------
# ---------------------------------------------------------------------------

output = create_output()

# ---------------------------------------------------------------------------
# process footprints into first sheet of output
# ---------------------------------------------------------------------------

with open('footprints.xml') as f:
    it = itertools.chain('<root>', f, '</root>')
    xml_root = ET.fromstringlist(it)

num_columns = [1, 2, 3]

sheet = create_sheet(output, 'footprints', 'Footprints')

# create header row ---------------------------------------------------------
col_count = 0
row_count = 0

for footprint in xml_root.findall('./footprint'):
    for item in footprint:
        write_cell(sheet, row_count, col_count, item.tag)
        col_count += 1
    write_cell(sheet, row_count, col_count, 'nozzle')
    break   # we only need to read the first one to generate the header

# create data rows ----------------------------------------------------------
row_count = 1
col_count = 0

for stack in xml_root.findall('./footprint'):
    for item in stack:
        item_count = list(stack).index(item)
        value = item.text
        value = value.replace('"', '')
        if not value in ['default']:    # note sure if we should skip 'null' as well
            convertible = (item_count in num_columns)
            write_cell(sheet, row_count, col_count, value, convertible)
        col_count += 1
    write_cell(sheet, row_count, col_count, 65, True)  # default nozzle
    row_count += 1
    col_count = 0


# ---------------------------------------------------------------------------
# process stack and special_stack in next 2 sheets of output
# ---------------------------------------------------------------------------

with open('stack_rp2040.xml') as f:
    it = itertools.chain('<root>', f, '</root>')
    xml_root = ET.fromstringlist(it)

sections = ['./stack', './special_stack']    
num_columns = [[2,3,5,6,7,8,9,15,16], [2,4,5,6,7,8,14,17]]
split_columns = [[4], [3, 15, 16]]
sheet = create_sheet(output, 'feeders', 'Stacks')

for section in range(2):
    # create header row ---------------------------------------------------------
    for stack in xml_root.findall(sections[section]):
        col_count = 0
        row_count = 0

        write_cell(sheet, row_count, col_count, 'id', True)
        col_count += 1

        for item in stack:
            item_count = list(stack).index(item)
            print(f"Column[{item_count:02d}]={item.tag}")
            if item_count in split_columns[section]:
                write_cell(sheet, row_count, col_count, item.tag+'_x')
                col_count += 1
                write_cell(sheet, row_count, col_count, item.tag+'_y')
                col_count += 1
            else:
                write_cell(sheet, row_count, col_count, item.tag)
                col_count += 1

        # add extra columns to the end
        write_cell(sheet, row_count, col_count, 'pnp')
        break   # we only need to read the first one to generate the header
 
    # create data rows ----------------------------------------------------------
    row_count = 1
    col_count = 0
    for stack in xml_root.findall(sections[section]):
        write_cell(sheet, row_count, col_count, stack.attrib['id'], True)
        col_count += 1

        for item in stack:
            item_count = list(stack).index(item)
            value = item.text

            if item_count in split_columns[section]:
                value1 = value.split('/')[0]
                value2 = value.split('/')[1]
                write_cell(sheet, row_count, col_count, value1, True)
                col_count += 1
                write_cell(sheet, row_count, col_count, value2, True)
                col_count += 1
                continue
            value = value.replace('"', '')
            if not value in ['default']:    # note sure if we should skip 'null' as well
                convertible = (item_count in num_columns[section])
                write_cell(sheet, row_count, col_count, value, convertible)
            col_count += 1
        write_cell(sheet, row_count, col_count, 1, True)   # default pnp machine
        row_count += 1
        col_count = 0
    if section == 0:
        # after first section, add a sheet and a table
        sheet = create_sheet(output, 'trays', 'Special Stacks')


# ---------------------------------------------------------------------------
# write out results to file
# ---------------------------------------------------------------------------

save_output(output, 'components')
