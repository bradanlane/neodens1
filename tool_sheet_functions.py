OUTPUT_FORMAT = 'numbers'  # 'xlsx' or 'numbers'

import os
import sys
import csv

# ---------------------------------------------------------------------------
if OUTPUT_FORMAT == 'numbers':
    import numbers_parser as np
else:
    from openpyxl import wb
import json

cell_style = None
    
# ---------------------------------------------------------------------------

def lookup_substitution(db, name):
    
    for substitution in db.substitutions:
        if name == substitution[0]:
            return substitution[1]
    return name


first_output_sheet = True   # global to know if there is a default sheet to write to

def create_output():
    global cell_style
    if OUTPUT_FORMAT == 'numbers':
        output = np.Document(num_header_cols=0, num_rows=1, num_cols=1)
        cell_style = output.add_style(
            name="Fixed Text",
            font_name="Courier New",
            font_size=10.0,
            alignment=np.Alignment("left", "top"),
        )
    else:
        output = wb()
    return output


def create_sheet(doc, sheet_name, table_name, data, titles, is_nums, widths):
    global first_output_sheet

    if OUTPUT_FORMAT == 'numbers':
        # numbers has a workbook with 1 or more sheets which may have 1 or more tables
        if first_output_sheet:
            first_output_sheet = False
            sheet = doc.sheets[0]
            sheet.name = sheet_name
            table = sheet.tables[0]
            table.name = table_name
        else:
            doc.add_sheet(sheet_name, table_name, num_rows=1, num_cols=1)
            sheet = doc.sheets[sheet_name]
            table = sheet.tables[0]
            table.num_header_cols = 0
    else:
        # excel has a workbook with 1 or more sheets with no tables
        table = doc.active
        table.title = sheet_name

    total_columns = len(titles)

    # set the column widths for count, component, footprint, nozzle, feeder, pnp
    for col_index, width in enumerate(widths):
        set_col_width(table, col_index, width)
    
    for col_index, title in enumerate(titles):
        write_cell(table, 0, col_index, title)

    for row_index, row in enumerate(data):
        for col_index, value in enumerate(row):
            #print(f"Writing {value} to {row_index+1}, {col_index}")
            write_cell(table, row_index+1, col_index, value, is_nums[col_index])

    return


def set_col_width(table, col_index, width):
    if OUTPUT_FORMAT == 'numbers':
        table.col_width(col_index, width)
    else:
        print("Error: set_col_width() not implemented for Excel files.")
        sys.exit()
        
def write_cell(table, row, col, value, is_convertible = False):
    global cell_style
    
    if isinstance(value, str):
        v = value.replace('"', '')
        if is_convertible:
            try:
                v = int(v)
            except:
                try:
                    v = float(v)
                except:
                    v = value.replace('"', '')
                    pass
    else:
        v = value
    if OUTPUT_FORMAT == 'numbers':
        table.write(row, col, v)
        if cell_style:
            table.set_cell_style(row, col, cell_style)
    else:
        table.cell(row=row+1, column=col+1).value = v

def get_table_data(table, skip_astrisk=False):
    results = []
    first_row = True
    if OUTPUT_FORMAT == 'numbers':
        for row in table.rows():
            # skip header row
            if first_row:
                first_row = False
                continue
    
            result = [cell.value for cell in row]
            if all(cell is None for cell in result):
                #print(f"Skipping empty row: {result}")
                continue
            if skip_astrisk and result[2] and result[2].startswith('*'):
                #print(f"Skipping row with *: {result}")
                continue
            results.append(result)
    else:
        print("Error: get_table_data() not implemented for Excel files.")
        sys.exit()
    return results


def get_cell_value(table, row, col):
    if OUTPUT_FORMAT == 'numbers':
        return table.cell(row, col).value
    else:
        return table.cell(row=row+1, column=col+1).value


def find_stack_by_name(db, pnp, name):
    for item in db.inventory:
        if item[1] == name and item[0] == pnp:
            #print(f"Found stack {name} at {item[0]}", item)
            if not isinstance(item[1], str):
                item[1] = int(item[1])
            return item
    return None


# ---------------------------------------------------------------------------
# ---------------------------------------------------------------------------

# Read in existing components data from the spreadsheets

def load_components():
    components = type('', (), {})()  # Create a simple object
    
    components_file = os.path.join(sys.path[0], 'components')
    if os.path.exists(components_file+"."+OUTPUT_FORMAT):
        table = load_sheet(components_file, 'footprints')
        components.footprints = get_table_data(table)

        # read in the substitutions
        table = load_sheet(components_file, 'substitutions')
        components.substitutions = get_table_data(table)

        # read in the components
        table = load_sheet(components_file, 'feeders')
        components.inventory = get_table_data(table, True)
        table = load_sheet(components_file, 'trays')
        trays = get_table_data(table, True)
        # Combine inventory and trays
        components.inventory.extend(trays)
        
        components.toolpath = sys.path[0]
        components.projectpath = os.getcwd()
        
        return components
    
    print(f"Error: 'components.{OUTPUT_FORMAT}' file not found.")
    sys.exit(1)


def load_sheet(filename, sheet_name):
    #print(f"load_sheet({filename}.{OUTPUT_FORMAT}, {sheet_name})")
    if OUTPUT_FORMAT == 'numbers':
        doc = np.Document(filename + '.numbers')
        sheet = doc.sheets[sheet_name]
        table = sheet.tables[0]
        
        return table
    else:
        doc = wb(filename + '.xlsx')
        sheet = doc[sheet_name]
        return sheet


def save_output(doc, filename):
    print(f"\nWriting {filename}.{OUTPUT_FORMAT} ...")
    if OUTPUT_FORMAT == 'numbers':
        doc.save(filename+'.numbers')
    else:
        doc.save(filename+'.xlsx')
  
def load_and_process_csv(db, args, sorting = 1, deduplicate=True):
    projectpath = ''
    
    # open the input file
    # try in the current directory and then in the script directory, 
    # then try as a fully qualified path
    
    paths = (db.toolpath, db.projectpath, '')
    #print(f"Looking for {args.infile} in {paths}")
    fin = None
    for path in paths:
        fullpath = os.path.join(path, args.infile)
        try:
            fin = open(fullpath, newline='')
            #print ("%-10s %-24s found in %s" % ("SOURCE", args.infile, path))
            projectpath = os.path.dirname(fullpath)
            break
        except (OSError, IOError) as e:
            #print(f"Attempted to open '{fullpath}' but failed", e)
            pass
    if not fin:
        print ("Error: could not open/read file: %s" % (args.infile))
        sys.exit()

    if projectpath != db.projectpath:
        print(f"Warning: Project path changed from {db.projectpath} to {projectpath}")
        db.projectpath = projectpath
        
    reader = csv.reader(fin)
    
    print ("\nProcessing %s to %s ..." % (args.infile, args.outfile))

    """
    this KiCAD CSV position file: 'Ref', 'Val', 'Package', 'PosX', 'PosY', 'Rot', 'Side'
    
    the first 4 columns are for reference:
        ref is unique
        val is the component
        package is the footprint

    same components are totaled to help optimize feeder placement
    """
    
    row = next(reader)  # discard the header row
    
    count = 0
    components = []
    need_install = []
    nozzles = []
    fiducials = []
    
    # read all rows of the KiCAD CSV and build the components list
    for row in reader:
        #print("processing", row)
        count += 1

        row[1] = lookup_substitution(db, row[1])    # possibly change the component name
        row[2] = lookup_substitution(db, row[2])    # possibly change the footprint name
        #print("cleaned   ", row)

        if row[1] == 'Fiducial':	# if 'Fiducial' in row[1]
            processed = False
            # if the fiducial is from KiKit and we placing 'panel at a time' then add it to the fiducials list
            if (row[2] == 'Fiducial' and args.format == 'panel'):
                fiducials.append([float(row[3]), float(row[4]), row[0]])
                processed = True
            # if the fiducial is from the PCB and we are placing 'board at a time' then add it to the fiducials list
            elif (row[2] != 'Fiducial' and args.format == 'board'):
                fiducials.append([float(row[3]), float(row[4]), row[0]])
                processed = True
            if processed:
                continue
        
        if not row[2]:
            print(f"skipping component: {row[1]:12s}  - missing footprint")
            continue

        if row[2] == '<SKIP>':
            #print(f"skipping component {row[1]:12s} - marked for skipping")
            continue
        
        if deduplicate:
            duplicate_component = False
            for component in components:
                #print("checking", component)
                # check for duplicate components
                if component[1] == row[1]:
                    component[0] += 1
                    duplicate_component = True
                    #print("updating", component)
                    break
            if duplicate_component:
                continue
        # end if deduplicate
        
        
        nozzle = 0
        for footprint in db.footprints:
            if row[2] == footprint[0]:
                nozzle = footprint[4]
                break
        if nozzle == 0:
            print(f"No Nozzle Found:  {row[1]:10s} with footprint {row[2]:40s}   Skipping")
            continue # sys.exit()

        pnp = 0
        feeder_position = 0
        in_inventory = False
        for part in db.inventory:
            if row[1] == part[2]: # if the component is in inventory, then use its feeder and pnp values
                pnp = part[0]
                feeder_position = part[1]
                in_inventory = True
                break
        if not in_inventory:
            # this is now reported later as a table
            #print(f"Not in Inventory: {row[1]:18s} with Footprint {row[2]:12s}")
            pass

        # determine if the nozzle position within the machine head is significant
        if feeder_position > 0:
            if feeder_position in [1, 2, 57, 58]:
                pos = [4]
            elif feeder_position in [3, 4, 55, 56]:
                pos = [4, 3]
            elif feeder_position in [5, 6, 53, 54]:
                pos = [4, 3, 2]
            elif feeder_position in [28, 29, 30, 31]:
                pos = [1]
            elif feeder_position in [26, 27, 32, 33]:
                pos = [1, 2]
            elif feeder_position in [24, 25, 34, 35]:
                pos = [1, 2, 3]
            elif feeder_position in range(160, 170):
                pos = [1]
            elif feeder_position in range(170, 180):
                pos = [1, 2]
            else:
                pos = [1, 2, 3, 4]
        if deduplicate:
            nozzles.append((int(pnp), int(nozzle), pos, pos)) # the second copy of pos is used for arbitration of ambiguous nozzle assignments
        else:
            nozzles.append((int(pnp), int(feeder_position), int(nozzle), pos))
            
        row[3] = float(row[3])  #PosX
        row[4] = float(row[4])  #PosY
        row[5] = float(row[5])  #Rot
        
        if deduplicate:
            # count, component, footprint, nozzle, feeder, pnp, coord_x, coord_y, rotation, skip
            components.append([1, row[1], row[2], int(nozzle), feeder_position, pnp, row[3], row[4], row[5], 'false'])
        else:
            # pnp, feeder_position, reference, value, nozzle, coordinates, rotation, str(skip), output_order
            components.append([int(pnp), row[0], row[1], int(feeder_position), int(nozzle), row[3], row[4], row[5], 'false', 0])

        if feeder_position == 0 or pnp == 0:
            need_install.append(components[-1])
        #print("adding  ", components[-1])
    # end for row in reader

    # Sort components first by nozzle and then by count
    if deduplicate:
        # sort 
        components.sort(key=lambda x: (x[5], x[4], -x[0]))
    else:
        components.sort(key=lambda x: (x[0], x[2]))


    # Sort nozzles by pnp and then by nozzle
    # the nozzles list contains tuples of pnp, nozzle, positions (and a second copy of positions)
    # where positions is a list of the acceptable positions on the machine head
    
    nozzles.sort(key=lambda x: (x[0], x[1]))

    #print("Initial nozzle assignments:")
    #for i, nozzle in enumerate(nozzles): print(f"{i:02d} {nozzle}")
    #print()
    
    if deduplicate: # we can only do nozzle reduction if we have removed duplicate components
        # reduce nozzle assignments by processing required nozzle positions
        #print("\nReducing nozzle assignments by processing required nozzle positions ...")
        reduction = True
        iteration = 1
        while reduction:
            reduction = False
            #print(f"\nIteration #{iteration} to reduce nozzle assignments")
            for i, nozzle in enumerate(nozzles):
                # create variables for the parts of the nozzle list
                pnp, nozzle_num, positions, originals = nozzle
                #print(f"component {(i+1):02d} is on PnP{pnp} with {nozzle_num} accessible from {positions} ...")

                if len(positions) == 1: # if there is only one acceptable position; we must set that position to that nozzle
                    positions = positions[0]
                    #print(f"                        requires {nozzle_num} in head position {positions}")
                    # remove the required nozzle position from the available other positions
                    for j, other_nozzle in enumerate(nozzles):
                        if other_nozzle[0] == pnp:
                            if len(other_nozzle[2]) > 1:
                                #print(f"                        removing head position {positions} from {str(other_nozzle[2]):14s}", end=' ')
                                new_positions = [pos for pos in other_nozzle[2] if pos != positions]
                                #print(f"results in {str(new_positions):14s} reserving {str(other_nozzle[3])}")
                                if len(new_positions) < len(other_nozzle[2]):
                                    reduction = True
                                nozzles[j] = [other_nozzle[0], other_nozzle[1], new_positions, other_nozzle[3]]
            iteration += 1
        
        #for i, nozzle in enumerate(nozzles):
        #    print(f"component {(i+1):02d} is on PnP{nozzle[0]} with {nozzle[1]} in {nozzle[2]}")

        #print("After first reduction:")
        #for i, nozzle in enumerate(nozzles): print(f"{i:02d} {nozzle}")
        #print()

        # further reduce nozzle assignments by processing ambiguous nozzle sizes
        #print("\nFurther reducing nozzle assignments by processing ambiguous nozzle sizes ...")
        reduction = True
        iteration = 1
        while reduction:
            reduction = False
            for i, nozzle in enumerate(nozzles):
                pnp, nozzle_num, positions, originals = nozzle
                if len(positions) > 1:
                    #print(f"{i:02d}: PnP{pnp} need arbitration for nozzle {nozzle_num} head positions {positions} originally {originals}")
                    # arbitrarily take the first position adn then remove it from the remaining
                    chosen_position = positions[0]

                    for j, other_nozzle in enumerate(nozzles):
                        if i == j:  # don't reduce the nozzle we just chose
                            continue
                        if other_nozzle[0] == pnp:
                            if len(other_nozzle[2]) > 1:
                                #print(f"    removing head position {chosen_position} from {str(other_nozzle[3]):14s}", end=' ')
                                new_positions = [pos for pos in other_nozzle[3] if pos != chosen_position]
                                #print(f"results in {str(new_positions):14s} reserving {str(other_nozzle[3])}")
                                if len(new_positions) < len(other_nozzle[2]):
                                    reduction = True
                                    nozzles[j] = [other_nozzle[0], other_nozzle[1], new_positions, new_positions]
                                    #print(f"    {j:02d}: {nozzles[j]}")
                    '''
                    for other_nozzle in nozzles:
                        if other_nozzle[0] == pnp and other_nozzle[1] == nozzle_num and len(other_nozzle[2]) == 1:
                            chosen_position = other_nozzle[2][0]
                            #print(f"Arbitrating: PnP{pnp} to use nozzle {nozzle_num} in head position {chosen_position}")
                            positions = other_nozzle[2]
                            nozzles[i] = [other_nozzle[0], other_nozzle[1], positions, other_nozzle[3]]
                            reduction = True
                            break
                    '''
                    
            iteration += 1
            
        #print("After second reduction:")
        #for i, nozzle in enumerate(nozzles): print(f"{i:02d} {nozzle}")
        #print()
        
        # if there are still ambiguous nozzle assignments, then check if the number of needed nozzle sizes is <= the number of ambiguous nozzle assignments
        # if so, then assign the nozzle sizes to the ambiguous nozzle assignments
        #print("\nAssigning nozzle sizes to ambiguous nozzle assignments by first available ...")
        reduction = True
        iteration = 1
        while reduction:
            reduction = False
            for i, nozzle in enumerate(nozzles):
                pnp, nozzle_num, positions, originals = nozzle
                if len(positions) > 1:
                    #print(f"PnP{pnp} need arbitration for nozzle {nozzle_num} head positions {positions} originally {originals}")
                    # assign the nozzle size to the first position
                    positions = [positions[0]]
                    #print(f"Arbitrating: PnP{pnp} to use nozzle {nozzle_num} in head position {positions}")
                    nozzles[i] = [pnp, nozzle_num, positions, originals]

                    # now remove the nozzle size from the other positions
                    for j, other_nozzle in enumerate(nozzles):
                        if other_nozzle[0] == pnp:
                            if positions[0] in other_nozzle[2] and len(other_nozzle[2]) > 1:
                                new_positions = [pos for pos in other_nozzle[2] if pos != positions[0]]
                                #print(f"                        removing head position {positions[0]} from {str(other_nozzle[2]):14s}", end=' ')
                                #print(f"results in {str(new_positions):14s} reserving {str(other_nozzle[3])}")
                                if len(new_positions) < len(other_nozzle[2]):
                                    reduction = True
                                nozzles[j] = [other_nozzle[0], other_nozzle[1], new_positions, other_nozzle[3]]
                            
            iteration += 1


        # clean up nozzle assignments
        #print("\nCleaning up nozzle assignments ...")
        for i, nozzle in enumerate(nozzles):
            pnp, nozzle_num, positions, originals = nozzle
            if len(positions) == 1:
                positions = positions[0]
                nozzles[i] = [pnp, nozzle_num, positions, originals]
            else:
                print(f"Warning: PnP{pnp} nozzle {nozzle_num} has ambiguous head positions {positions}")

        # Sort nozzles by PnP and then by nozzle number
        nozzles.sort(key=lambda x: (x[0], -x[2], x[1]))

        # Remove duplicates from nozzles
        unique_nozzles = []
        seen = set()
        for nozzle in nozzles:
            if (nozzle[0], nozzle[1], nozzle[2]) not in seen:
                unique_nozzles.append(nozzle)
                seen.add((nozzle[0], nozzle[1], nozzle[2]))
        nozzles = unique_nozzles
    # end on nozzle reduction (which only works if we removed duplicate components)
    
    
    # report any components that need to be installed on a machine
    if len(need_install) > 0:
        print("\nThe following components need to be installed:")
        print("-"*50)
        print(f"{'Component':18s} {'Footprint':12s} {'Nozzle':6s} {'Feeder':6s} {'PnP':4s}")
        print("-"*50)
        for i, component in enumerate(need_install):
            print(f"{component[1]:18s} {component[2]:12s} {f"{component[3]:03d}":>6s} {str(component[4]):>6s} {'?':>3s}")
    else:
        print("\n*** All components are in inventory and installed ***")
    print()

    fin.close() # close the input CSV file

    db.components = components
    db.fiducials = fiducials
    db.nozzles = nozzles
    db.need_install = need_install
    # db.projectpath = projectpath
    return

def load_predefined_fiducials(db, name):

    paths = (db.toolpath, db.projectpath, '')
    #print(f"Looking for {name} in {paths}")
    fin = None
    for path in paths:
        fullpath = os.path.join(path, name)
        try:
            fin = open(fullpath, newline='')
            #print ("%-10s %-24s found in %s" % ("SOURCE", name, path))
            break
        except (OSError, IOError) as e:
            #print(f"Attempted to open '{fullpath}' but failed", e)
            pass
    if not fin:
        print ("Info: could not open/read file: %s" % (name))
        db.defined_fiducials = None
        return

    data = json.load(fin)

    fiducials = []
    for item in data:
        fiducial = {
            'name': item['name'],
            'pnp': item['pnp'],
            'image': item['image']
        }
        fiducials.append(fiducial)

    db.defined_fiducials = fiducials
    fin.close()
    return
