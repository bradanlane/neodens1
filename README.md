# neoden

The `components.numbers` file contains a table of all parts on reels on feeders and in trays.
It also contains other data, including a table of component footprints, and a text substitution table.

## Step 1: tool_kicad2mapper.py

The KiCAD-2-Mapper is the first step. It ingests a KiCAD positions file and uses the `components` data
to determine inventory status:

1. identify components which are already installed on the machine(s)
1. identify components which are available on feeders
1. identify components which are not listed in inventory

The tool generates a spreadsheet with two tables.
The first is a best-guess for which nozzles need to be install on the machine(s) and which head position to to use for each nozzle.
The second table contains a list of all components with their machine and feeder positions.
Component feeders which need to be installed are listed with a zero (0) for both the machine and feeder position.

This step is concluded after running the mapper and installing any needed feeders and nozzles.

## Step 2: tool_kicad2job.py

The KiCAD-2-Job is the secondt step. It ingests several files:

1. the `components` data
1. the project job configuration JSON
1. the KiCAD positions file CSV
1. the project mapper file

