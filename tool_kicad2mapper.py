# tool_kicad2components.py (CSV file converter for TronStol A1)
# Copyright: 2024 Bradan Lane STUDIO
# Open Source License: MIT

""" --------------------------------------------------------------------------

kicad2components.numbers for TronStol A1 (aka Neoden S1)

This tool will generate a spreadsheet for all of the components on a PCB.
It will include the number of each component and the nozzle that is required.

For components already installed on a machine, it will set defaults for
the machine and feeder position.

The administrator will update this file as they install feeders onto one or
more pick-and-place machines.

The resulting file will be used to generate the machine job file(s).

The tool will also verify if the component is in inventory and generate
a list of components that need to be ordered and/or installed onto feeders.


The updated map file will be used by kicad2job to update the components
spreadsheet. The map file will also be used to generate the machine job file(s).

inputs: a kicad position file (CSV)
outputs: a spreadsheet for machine setup

-------------------------------------------------------------------------- """

DEFAULT_OUTPUT_FILE = 'mapper'

import os

import argparse
import json

import tool_sheet_functions as tsf
import tool_fiducial_image as tfi

# ---------------------------------------------------------------------------
# ---------------------------------------------------------------------------

def write_output(db, args):
    # -----------------------------------------------------------------------
    # create the output file which will hold the nozzles and components tables
    # -----------------------------------------------------------------------
    output = tsf.create_output()
    
    # -----------------------------------------------------------------------
    # Write the nozzles to the output file
    # -----------------------------------------------------------------------

    print("\nConfigure the machines with the following nozzles:")
    print("-"*20)
    print(f"{'PnP':3s} {'Nozzle':>6s} {'Position':>8s}")
    print("-"*20)
    for i, nozzle in enumerate(db.nozzles):
        print(f"{(int(nozzle[0])):3d}    {int(nozzle[1]):03d} {nozzle[2]:8d}")
        db.nozzles[i] = [nozzle[0], nozzle[2], f"{int(nozzle[1]):03}"]

    tsf.create_sheet(output, 'nozzles', 'Nozzles', 
                     db.nozzles, 
                     ['pnp', 'position', 'nozzle'], 
                     [True, True, False],
                     [30, 80, 60])


    # -----------------------------------------------------------------------
    # write the components to the output file
    # -----------------------------------------------------------------------

    for i, component in enumerate(db.components):
        db.components[i] = [component[0], component[1], component[2], f"{int(component[3]):03d}", component[4], component[5]]

    table = tsf.create_sheet(output, 'components', 'Components', 
                             db.components, 
                             ['cnt', 'component', 'footprint', 'nozzle', 'feeder', 'pnp'], 
                             [True, False, False, False, True, True],
                             [30, 200, 80, 50, 50, 30])

    # output to the same folder as the input file
    outfilefull = os.path.join(db.projectpath, args.outfile)
    tsf.save_output(output, outfilefull)



def parse_args_simple():
    cmdline_args = argparse.ArgumentParser()
    cmdline_args.add_argument("infile", help="name of KiCAD CSV position file")
    cmdline_args.add_argument("outfile", nargs='?', help="(optional) base name of spreadsheet file without extension", default=DEFAULT_OUTPUT_FILE)

    args = cmdline_args.parse_args()
    return args

def main():
    args = parse_args_simple()
    print(args)
    
    # Read in existing components data from the spreadsheets
    db = tsf.load_components()

    tsf.load_and_process_csv(db, args, deduplicate=True) # adds to db
    write_output(db, args)
    
    print()

# start execution here
main()
